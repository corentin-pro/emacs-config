(require 'package)
(add-to-list 'package-archives
  '("melpa" . "http://melpa.org/packages/")
  '("melpa-stable" . "https://stable.melpa.org/packages/"))

(package-initialize)

;; package to install
(setq package-list '(company markdown-preview-mode markdown-mode eterm-256color
  visual-regexp multi-term all-the-icons helm-projectile helm neotree
  projectile elpy yasnippet-snippets s pyvenv find-file-in-project evil company
  all-the-icons company-tern rjsx-mode evil))

;; elpy might not installed properly, manual installation seems the only way...

;; Fetch the list of packages available
;; (unless package-archive-contents
(or (file-exists-p package-user-dir)
  (package-refresh-contents))

;; Install the missing packages
(dolist (package package-list)
  ;(message "Checking id `%s` is installed" (package))
  (unless (package-installed-p package)
  ;(message "Installing `%s`..." (package))
  (package-install package)))


;; -------- Emacs config --------

(setq inhibit-startup-message t) ;; hide the startup message
(setq backup-directory-alist '(("" . "~/.emacs.d/emacs-backup"))) ;; set backup files to be written in the directoy (no tree, flat directory)
(setq make-backup-files nil) ;; stop creating backup~ files

(blink-cursor-mode 0) ;; disable blinking cursor
(tool-bar-mode 0) ;; disable the tool bar
(scroll-bar-mode 0) ;; disable scroll bar
;; (global-linum-mode 1) ;; enable line numbers at the left


(setq tramp-default-method "ssh") ;; tramp using ssh instead of slow scp

;; Changing default font
(add-to-list 'default-frame-alist '(font . "Hack-12"))
(set-face-attribute 'default t :font "Hack-12")

;; Change theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
;;(load-theme 'breeze-dark t)

;; Open init file (this file) shortcut
(defun open-init-file ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
(global-set-key [f12] 'open-init-file)

;; JSON pretty print shortcut
(global-set-key (kbd "C-c j") 'json-pretty-print-buffer)

;; Revert buffer shortcut
(global-set-key [f5] 'revert-buffer)

;; Shows white spaces
(global-whitespace-mode t)
;; set whitespace mode
(setq whitespace-style '(face trailing tabs spaces lines-tail empty indentation space-after-tab space-before-tab
										space-mark tab-mark))
;; whitespace max line length
(setq whitespace-line-column 200)

;; Tab width
(setq-default tab-width 3)

;; Recent files
(setq recentf-auto-cleanup 'never) ;; disable before we start recentf (because of tramp mode)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
;; (global-set-key "\C-x\ \C-r" 'recentf-open-files)
;; Save recent file every 5 minutes
(run-at-time nil (* 5 60) 'recentf-save-list)
;; Nice recent file menu
(load "~/.emacs.d/recentf-minibuffer.el")
(global-set-key "\C-x\ \C-r" 'recentf-minibuffer-dialog)


;; -------- Mode Hooks  --------

(defun my-python-mode-hook ()
  (linum-mode 1)
  (setq whitespace-line-column 120))
(add-hook 'python-mode-hook 'my-python-mode-hook)

(defun lisp-company-hook ()
  (linum-mode 1)
  ;; (whitespace-mode 1)
  (company-mode 1))
(add-hook 'emacs-lisp-mode-hook 'lisp-company-hook)


;; -------- Evil Mode --------
(require 'evil)
(evil-mode 1)

;; -------- Company --------

;; Avoid whitespace inside company tooltips
(defvar my-prev-whitespace-mode nil)
(make-variable-buffer-local 'my-prev-whitespace-mode)
(defun pre-popup-draw ()
  "Turn off whitespace mode before showing company complete tooltip"
  (if global-whitespace-mode
		(progn
		  (setq my-prev-whitespace-mode t)
		  (global-whitespace-mode -1)
		  (setq my-prev-whitespace-mode t))))
(defun post-popup-draw ()
  "Restore previous whitespace mode after showing company tooltip"
  (if my-prev-whitespace-mode
		(progn
		  (global-whitespace-mode 1)
		  (setq my-prev-whitespace-mode nil))))
(advice-add 'company-pseudo-tooltip-unhide :before #'pre-popup-draw)
(advice-add 'company-pseudo-tooltip-hide :after #'post-popup-draw)


;; -------- Icons  --------

(require 'all-the-icons)


;; -------- Eterm-256color  --------

(add-hook 'term-mode-hook #'eterm-256color-mode)


;; -------- iBuffer  --------

(require 'ibuffer)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)

;; Use human readable Size column instead of original one
(define-ibuffer-column size-h
  (:name "Size" :inline t)
  (cond
  ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
  ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
  ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
  (t (format "%8d" (buffer-size)))))

;; Modify the default ibuffer-formats
(setq ibuffer-formats
		'((mark modified read-only " "
				  (name 18 18 :left :elide)
				  " "
				  (size-h 9 -1 :right)
				  " "
				  (mode 16 16 :left :elide)
				  " "
				  filename-and-process)))


;; -------- Helm --------

(require 'helm-config)
(global-set-key (kbd "M-x") 'helm-M-x)


;; -------- Elpy --------

(elpy-enable) ;; enable elpy (python dev package)
(setq elpy-rpc-python-command "python") ;; using python for Remote Procedure Call (python run in the background)
(setq python-shell-interpreter "ipython"
		python-shell-interpreter-args "-i --simple-prompt") ;; using ipython
(pyvenv-activate "~/.virtualenv")

;; Delete trailing spaces before saving
(add-hook 'elpy-mode-hook
			 (lambda ()
				(add-hook 'before-save-hook 'delete-trailing-whitespace nil t)))
(global-set-key (kbd "C-#") 'comment-region)
(global-set-key (kbd "M-#") 'uncomment-region)

;; -------- Projectile --------

(require 'projectile)
(projectile-global-mode) ;; enable projectile on all buffers by default
(setq projectile-enable-caching t)


;; -------- Helm-Projectile --------

(require 'helm-projectile)
(helm-projectile-on) ;; replace projectile auto-complete (ido style) by helm ones

;; -------- Neotree --------

(require 'neotree)
;; (global-set-key [f9] 'neotree-toggle) ;; simple neotree use
(defun neotree-project-dir ()
  "Open NeoTree using the git root."
  (interactive)
  (let ((project-dir (ignore-errors projectile-project-root))
		  (file-name (buffer-file-name)))
	 (if (neo-global--window-exists-p)
		  (neotree-hide)
		(progn
	(neotree-show)
	(if project-dir
		 (neotree-dir project-dir))
	(if file-name
		 (neotree-find file-name))))))
(global-set-key [f9] 'neotree-project-dir)
(setq neo-autorefresh nil)
;; changing neotree root directory on projectile-switch-project
(setq projectile-switch-project-action 'neotree-projectile-action)

(setq neo-theme 'icons)
;; Need to 'M-x all-the-icons-install-fonts' once to install the font in the system

;; -------- Multi-Term --------

(require 'multi-term)
(setq multi-term-program "/bin/zsh") ;; set default shell
;; Set buffer size to 10k lines
(add-hook 'term-mode-hook
			 (lambda ()
				(setq term-buffer-maximum-size 10000)))
(global-set-key [f10] 'multi-term)


;; -------- Javascript --------

(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

(setq js2-strict-missing-semi-warning nil)

;; Indentation
(setq js-indent-level 3)

;; Set company-tern for autocompletion
(require 'company)
(require 'company-tern)

(add-to-list 'company-backends 'company-tern)
(add-hook 'js2-mode-hook (lambda ()
									(linum-mode)
									(tern-mode)
									(company-mode)))


;; -------- Custom  --------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
	[default default default italic underline success warning error])
 '(ansi-color-names-vector
	["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (breeze-dark)))
 '(custom-safe-themes
	(quote
	 ("a51d3151e56919a394005bd129f50156fe60ac3c4816de5c1e041e7f55ff13a0" default)))
 '(fci-rule-color "#2e2e2e")
 '(package-selected-packages
	(quote
	 (rainbow-mode elpygen js2-mode company-tern markdown-preview-mode markdown-mode eterm-256color visual-regexp multi-term helm-projectile helm elpy yasnippet-snippets s pyvenv find-file-in-project evil company)))
 '(vc-annotate-background "#3b3b3b")
 '(vc-annotate-color-map
	(quote
	 ((20 . "#dd5542")
	  (40 . "#CC5542")
	  (60 . "#fb8512")
	  (80 . "#baba36")
	  (100 . "#bdbc61")
	  (120 . "#7d7c61")
	  (140 . "#6abd50")
	  (160 . "#6aaf50")
	  (180 . "#6aa350")
	  (200 . "#6a9550")
	  (220 . "#6a8550")
	  (240 . "#6a7550")
	  (260 . "#9b55c3")
	  (280 . "#6CA0A3")
	  (300 . "#528fd1")
	  (320 . "#5180b3")
	  (340 . "#6380b3")
	  (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3"))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
