(deftheme breeze-dark
  "Created 2018-05-28.")

;;; Color Palette

(defvar breeze-dark-colors-alist
  '(("breeze-dark-bg-main"     . "#21262b")))

(let ((dark-bg             "#21262b")
		(light-bg            "#31363b")
		(highlight-bg        "#41464b")
		(lighter-bg          "#61666b")
		(light-fg            "#ddd")
		(grey-fg             "#aaa")
		(dark-fg             "#666")
		(darker-fg           "#333")
		(error-bg            "#b43")
		(warning-bg          "#543")
		(error-fg            "#ccc")
		(selection-bg        "#448")
		(completion-fg       "#ddd")
		(completion-match-fg "#daa")
		(light-blue          "#188"))
  (custom-theme-set-faces
	'breeze-dark
	'(default ((t (:inherit nil
									:stipple nil
									:background "#21262b"
									:foreground "#d0d0d0"
									:inverse-video nil
									:box nil
									:strike-through nil
									:overline nil
									:underline nil
									:slant normal
									:weight normal
									:height 119
									:width normal
									:foundry "SRC"
									:family "Hack"))))
	`(cursor ((t (:background ,grey-fg :foreground ,darker-fg))))
	`(region ((t (:background ,selection-bg))))
	`(fringe ((t (:background ,dark-bg :foreground ,grey-fg))))
	`(linum  ((t (:background ,dark-bg :foreground ,grey-fg))))

	;; -------- Syntax --------
	`(font-lock-keyword-face ((t (:foreground ,light-blue))))

	;; -------- Mode Line --------
	`(mode-line ((t (:background ,lighter-bg :foreground ,light-fg))))
	`(mode-line-inactive ((t (:background ,light-bg :foreground ,light-fg))))

	;; -------- Company (autocompletion)--------
	`(company-preview-common               ((t (:background ,selection-bg :foreground ,light-fg))))
	`(company-scrollbar-bg                 ((t (:background ,highlight-bg))))
	`(company-scrollbar-fg                 ((t (:background ,light-blue))))
	`(company-tooltip                      ((t (:background ,light-bg :foreground ,completion-fg))))
	`(company-tooltip-annotation           ((t (:foreground ,completion-match-fg))))
	`(company-tooltip-annotation-selection ((t (:foreground ,completion-match-fg))))
	`(company-tooltip-common               ((t (:foreground ,completion-match-fg))))
	`(company-tooltip-selection            ((t (:background ,selection-bg))))

	;; -------- Whitespace-mode --------
	`(whitespace-big-indent       ((t (:background ,error-bg :foreground ,error-fg))))
	`(whitespace-empty            ((t (:background ,warning-bg :foreground ,nil))))
	`(whitespace-indentation      ((t (:background ,warning-bg :foreground ,dark-fg))))
	`(whitespace-line             ((t (:background ,warning-bg :foreground ,nil))))
	`(whitespace-newline          ((t (:background ,nil :foreground ,dark-fg))))
	`(whitespace-space            ((t (:background ,nil :foreground ,dark-fg))))
	`(whitespace-space-after-tab  ((t (:background ,warning-bg :foreground ,dark-fg))))
	`(whitespace-space-before-tab ((t (:background ,warning-bg :foreground ,dark-fg))))
	`(whitespace-tab              ((t (:background ,nil :foreground ,dark-fg))))
	`(whitespace-trailing         ((t (:background ,error-bg :foreground ,error-fg))))

	;; -------- Neo Tree --------
	`(neo-file-link-face ((t (:foreground ,light-fg))))
  )
)

(provide-theme 'breeze-dark)
